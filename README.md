#Install
`pip install -r requirements.txt`

#Usage
`generate.py -s <n_samples> -c <centers> -n <n_features> --show-plot`