from sklearn.datasets import make_blobs
from matplotlib import pyplot
from pandas import DataFrame
from numpy import savetxt
import getopt as go
import datetime as dt
import sys
import random


def generate_data(_n_samples, _centers, _n_features, _show_plot):
    x, y = make_blobs(n_samples=_n_samples, centers=_centers, n_features=_n_features)
    savetxt("data_" + str(dt.datetime.now().timestamp()) + ".csv", x, delimiter=" ")

    if _show_plot:
        df = DataFrame(dict(x=x[:, 0], y=x[:, 1], label=y))
        fig, ax = pyplot.subplots()
        grouped = df.groupby('label')
        for key, group in grouped:
            group.plot(ax=ax, kind='scatter', x='x', y='y', label=key, color=(random.random(),
                                                                              random.random(),
                                                                              random.random()))
        pyplot.show()


def show_help():
    print("\nUsage: generate.py -s <n_samples> -c <centers> -n <n_features> '--show-plot'")
    sys.exit(2)


if __name__ == "__main__":
    try:
        opts, args = go.getopt(sys.argv[1:], "s:c:n:", ["show-plot"])
    except go.GetoptError:
        show_help()
    n_samples = None
    centers = None
    n_features = None
    show_plot = False
    for o, a in opts:
        if o == "-s":
            n_samples = int(a)
        elif o == "-c":
            centers = int(a)
        elif o == "-n":
            n_features = int(a)
        elif o == "--show-plot":
            show_plot = True
        else:
            print("\nUnhandled option: " + o)
            sys.exit(2)
    if n_samples and centers and n_features is not None:
        generate_data(n_samples, centers, n_features, show_plot)
    else:
        show_help()
